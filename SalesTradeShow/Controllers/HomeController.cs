﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Mvc;
using SalesTradeShow.Models;
using System.Web.Script.Serialization;
using HME.SQLSERVER.DAL;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Collections;
using Microsoft.AspNet.Identity;

namespace hme.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        [Authorize(Roles = "User")]
        public ActionResult Index()
        {
            User user = (User)Session["user"];
            ldap ldap = new ldap();
            //var cc = User.Identity.GetUserName();
            //user = ldap.GetUserDetails(user);

            JavaScriptSerializer json = new JavaScriptSerializer();
            HomeModels homeModels = new HomeModels();
            var dto = homeModels.jsonStartup();
            ViewBag.dto = json.Serialize(dto);

            return View();
        }

        [HttpPost]
        public JsonResult StoreLookup(HomeModels.storesdto dto)
        {
            HomeModels homeModels = new HomeModels();
            homeModels.StoreLookup(dto);
            return Json(dto);
        }
    }
}