﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Mvc;
using Signature.Models;
using System.Web.Script.Serialization;
using HME.SQLSERVER.DAL;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Collections;

namespace Configurator.Controllers
{
    public class ConfiguratorController : Controller
    {
        // GET: Configurator
        public ActionResult Index()
        {       
            return View();
        }

        // GET: Configurator/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Configurator/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Configurator/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Configurator/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Configurator/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Configurator/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Configurator/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public JsonResult SaveSignature(string signatureData, int width, int height)
        {
            SignatureModels.dto dto = new SignatureModels.dto();
            SignatureImageSave saveimage = new SignatureImageSave();
            MemoryStream img = saveimage.SaveSignatureImage(signatureData);
            byte[] imageBytes = img.ToArray();
            dto.image = Convert.ToBase64String(imageBytes);
            string json = new JavaScriptSerializer().Serialize(dto);
            return Json(dto);
        }

    }

    public class SignatureImageSave
    {
        public MemoryStream SaveSignatureImage(string signatureData)
        {
            var stream = new MemoryStream();
            using (MemoryStream mem = new MemoryStream(Convert.FromBase64String(signatureData)))
            {
                Bitmap bm = (Bitmap)Image.FromStream(mem);
                Bitmap resized = new Bitmap((int)(0.3f * bm.Width), (int)(0.3f * bm.Height));
                Graphics g = Graphics.FromImage(resized);
                g.DrawImage(bm, new Rectangle(0, 0, resized.Width, resized.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel);
                g.Dispose();
                //string path = Path.Combine(HttpContext.Current.Server.MapPath("../images"), "99444.png");
                //resized.Save(path, ImageFormat.Png);
                resized.Save(stream, ImageFormat.Png);
                stream.Position = 0;
            }
            return stream;
        }
    }
}
