﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Mvc;
using Signature.Models;
using System.Web.Script.Serialization;

namespace SalesTradeShow.Controllers
{
    public class SignatureController : Controller
    {
        // GET: Signature
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult SaveSignature(string signatureData, int width, int height)
        {
            SignatureModels.dto dto = new SignatureModels.dto();
            SignatureModels.SignatureImageSave saveimage = new SignatureModels.SignatureImageSave();
            MemoryStream img = saveimage.SaveSignatureImage(signatureData);
            byte[] imageBytes = img.ToArray();
            dto.image = Convert.ToBase64String(imageBytes);
            string json = new JavaScriptSerializer().Serialize(dto);
            return Json(dto);
        }
    }
}
